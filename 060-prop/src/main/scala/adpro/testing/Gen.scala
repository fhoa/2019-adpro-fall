// Advanced Programming, A. Wąsowski, IT University of Copenhagen
//
// Group number: 27
//
// AUTHOR1: KRWA
// TIME1: 3 <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)
//
// AUTHOR2: FHOA
// TIME2: 3 <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)
//
// AUTHOR3: SEOL
// TIME3: 3 <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)
// You should work with the file by following the associated exercise sheet
// (available in PDF from the course website).
//
// The file shall always compile and run after you are done with each exercise
// (if you do them in order).  Please compile and test frequently. Of course,
// some tests will be failing until you finish. Only hand in a solution that
// compiles and where tests pass for all parts that you finished.    The tests
// will fail for unfnished parts.  Comment such out.
//
// Before starting to work on the exercises, familiarize yourself with the the
// content already in the file (it has been explained in chapter 8, but it is
// useful to see it all together in one file).

package adpro.testing
import fpinscala.state._
import fpinscala.state.RNG._

// A generator will use a random number generator RNG in its state, to create
// random instances (but perhaps also some other staff)
case class Gen[A] (sample :State[RNG,A]) {

  // Let's convert generator to streams of generators
  def toStream (seed: Long): Stream[A] =
    Gen.state2stream (this.sample) (RNG.Simple (seed))
  def toStream (rng: RNG): Stream[A] =
    Gen.state2stream (this.sample) (rng)

  // Exercise 3

  def listOfN (n: Int): Gen[List[A]] = {
    implicit val a : Gen[A] = (this)
    Gen(State.sequence(List.fill(n)(implicitly[Gen[A]].sample)))
  }

  // Exercise 4

  def flatMap[B] (f: A => Gen[B]): Gen[B] =
  Gen(State(rng => {
    val (v, rng2) = this.sample.run(rng)
    val Gen(s) = f(v)
    s.run(rng2)
  }))


  // It would be convenient to also have map  (uses flatMap)

  def map[B] (f: A => B): Gen[B] = this.flatMap (a => Gen.unit[B] (f(a)))

  // Exercise 5

  def listOfN (size: Gen[Int]): Gen[List[A]] = size.flatMap (s => {
    implicit val a : Gen[A] = (this)
    Gen(State.sequence(List.fill(s)(implicitly[Gen[A]].sample)))
  })


  // Exercise 6

  def union (that: Gen[A]): Gen[A] =
    Gen(State(rng =>{
      val (bool, rng2) = boolean(rng)
      val g = if(bool) this else that
      g.sample.run(rng2)
    }))

  // Exercise 7 continues in the companion object (below)
}

object Gen {

  // A convenience function to convert states (automata) to streams (traces)
  // It would be better to have it in State, but I am not controlling
  // State.scala.

  private  def state2stream[A] (s :State[RNG,A]) (seed :RNG) :Stream[A] =
    s.run(seed) match { case (n,s1) => n #:: state2stream (s) (s1) }

  // A generator for Integer instances

  def anyInteger: Gen[Int] = Gen(State(_.nextInt))

  // Exercise 1

  def choose (start: Int, stopExclusive: Int): Gen[Int] =
    Gen(State(rng => {
      val (v, rng2) = rng.nextInt
      (start + ((Math.abs(v) % (stopExclusive-start))), rng2)
    }))

  // Exercise 2

  def unit[A] (a: =>A): Gen[A] = Gen(State.unit(a))

  def boolean: Gen[Boolean] =
    Gen(State(rng => {
      val Gen(State(sg)) = choose(0, 2)
      val (v,rng2) = sg(rng)
      val bool = if(v == 1) true else false
      (bool, rng2)
    }))

  def double: Gen[Double] = Gen(State(RNG.double))

  // (Exercise 3 is found in the Gen class above)

}

// This is the Prop type implemented in [Chiusano, Bjarnasson 2015]

object Prop {

  type TestCases = Int
  type SuccessCount = Int
  type FailedCase = String

  // the type of results returned by property testing

  sealed trait Result { def isFalsified: Boolean }
  case object Passed extends Result { def isFalsified = false }
  case class Falsified(failure: FailedCase,
    successes: SuccessCount) extends Result {
      def isFalsified = true
  }
  case object Proved extends Result { def isFalsified = false }

  def forAll[A](as: Gen[A])(f: A => Boolean): Prop = Prop {
    (n,rng) => as.toStream(rng).zip(Stream.from(0)).take(n).map {
      case (a,i) => try {
        if (f(a)) Passed else Falsified(a.toString, i)
      } catch { case e: Exception => Falsified(buildMsg(a, e), i) }
    }.find(_.isFalsified).getOrElse(Passed)
  }

  def buildMsg[A](s: A, e: Exception): String =
    s"test case: $s\n" +
    s"generated an exception: ${e.getMessage}\n" +
    s"stack trace:\n ${e.getStackTrace.mkString("\n")}"
}

import Prop._

case class Prop (run: (TestCases,RNG)=>Result) {

  // (Exercise 7)

  def && (that: Prop): Prop = Prop {
    (n, rng) =>
      val result1 = this.run(n, rng)
      val result2 = that.run(n, rng)
      if(result1.isFalsified && result2.isFalsified) Falsified(result1.toString + result2.toString, 2)
      if(result1.isFalsified) Falsified(result1.toString, 1)
      if(result2.isFalsified) Falsified(result2.toString, 1)
      Proved
  }

  def || (that: Prop): Prop = Prop {
    (n, rng) =>
      val result1 = this.run(n, rng)
      val result2 = that.run(n, rng)
      if(result1.isFalsified && result2.isFalsified) Falsified(result1.toString + result2.toString, 2)
      Proved
  }

}

// vim:cc=80:foldmethod=indent:nofoldenable
