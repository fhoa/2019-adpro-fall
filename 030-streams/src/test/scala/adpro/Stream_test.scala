package adpro
import org.scalatest.FunSuite
import org.scalatest.Assertions._
import Stream._

class Stream_test extends FunSuite {

  val testVal = naturals.takeWhile(_<1000000000).take(1000)
  
  test("This one always works: (-1) * (-1) = 1") {
    assert((-1)*(-1)==1);
  }
  
  //Sanity check of exercise 1
  test("The first element of Stream.from(3) is 3") {
    assert(from(3).headOption().contains(3));
  }

  test("The second element of Stream.from(3) is 4") {
    assert(from(3).tail.headOption().contains(4));
  }

	test("The first element of Stream.to(3) is 3") {
    assert(to(3).headOption().contains(3));
  }
  
	test("The second element of Stream.to(3) is 2") {
    assert(to(3).tail.headOption().contains(2));
  }

	test("The first element of naturals is 0"){
		assert(naturals.headOption().contains(0))
	}


	test("The Stream(1,2,3).toList is List(1,2,3) "){
		  val l2 :Stream[Int]= cons(1, cons(2, cons (3, empty)))
			assert(l2.toList(0) == 1)
			assert(l2.toList(1) == 2)
			assert(l2.toList(2) == 3)


	}

	test("naturals.take(3)  is Stream(1,2,3) "){
		  val l2 :Stream[Int]= naturals.take(3) 
			assert(l2.toList(0) == 0)
			assert(l2.toList(1) == 1)
			assert(l2.toList(2) == 2)
  }

  test("naturals.drop(3) is Stream(3,4,5,...) "){
		  val l2 :Stream[Int]= naturals.drop(3) 
			assert(l2.headOption().contains(3))
      assert(l2.tail.headOption().contains(4))
      assert(l2.tail.tail.headOption().contains(5))

			//assert(l2.toList(2) == 5)
  }

  test("test take while"){
    val ls :List[Int]= naturals.takeWhile(_<1000).toList
    assert(ls.length == 1000)
  }

  test("test that only first 50 elements is taken"){
    val ls :List[Int]= naturals.takeWhile(_<1000000000).drop(100).take(50).toList
    assert(ls.length == 50)
  }

  test("test for all success"){
    val ls = naturals.forAll(_<0)
    assert(!ls)
  }

  test("test for all failure"){
    assertThrows[StackOverflowError] {
      naturals.forAll(_ >= 0)
    }
  }

  test("test take while2 - test that only first 50 elements is taken"){
    val ls :List[Int]= naturals.takeWhile2(_<1000000000).drop(100).take(50).toList
    assert(ls.length == 50)
  }


  test("test take while2 - test that only first 100 elements is taken"){
    val ls :List[Int]= testVal.takeWhile2(_<100).toList
    assert(ls.length == 100)
  }

  test("test headoption2"){
    val v :Option[Int]= testVal.headOption2()
    assert(v == Some(0))
  }

  test("test map"){
    val v :List[Int]= naturals.map (_*2).drop (30).take (50).toList
    assert(v.length == 50)
    assert(v.last == (79*2))
  }

  test("test filter"){
    val v :List[Int]= naturals.drop(42).filter (_%2 ==0).take (30).toList
    assert(v.length == 30)
    assert(v.head == 42)
    assert(v.last == 100)
  }

  test("test append"){
    val v :Stream[Int]= naturals.append (naturals) //Should not throw!
    val w :List[Int]= naturals.take(10).append(naturals).take(20).toList
    assert(w.length == 20)
    assert(w.head == 0)
    assert(w(0) == w(10))
    assert(w(9) == w(19))
  }

  test("test flatMap"){
    val v :List[Int] = naturals.flatMap (to _).take (100).toList
    assert(v(0) == 1)
    assert(v(1) == 2)
    assert(v(3) == 3)

  }

  test("test unfold"){
    val v:List[Int] = naturals.unfold ("___") ((s: String) => Some[(Int, String)](s.length(), s+"_")) .take(10).toList
    assert(v(0) == 3)
    assert(v(9) == 12)

  }

  test("from2"){
    val v:List[Int] = naturals.from2 (22).take(10).toList
    assert(v(0) == 22)
  }

  test("test map2"){
    val v :List[Int]= naturals.map2 (_*2).drop (30).take (50).toList
    assert(v.length == 50)
    assert(v.last == (79*2))
  }

  test("test take 15") {
    val v : List[Int] = naturals.take2 (15).toList
    assert(v.length == 15)
  }

  test("test take while3"){
    val ls :List[Int]= testVal.takeWhile2(_<100).toList
    assert(ls.length == 100)
  }

  test("zipWith"){
    val ls : List[Int] = naturals.zipWith[Int,Int] ((a: Int, b:Int) => a+b) (naturals).take(2000000000).take(20).toList
    assert(ls.length == 20)
  }


}