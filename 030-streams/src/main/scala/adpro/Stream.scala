// Advanced Programming
// Andrzej Wąsowski, IT University of Copenhagen
//
// meant to be compiled, for example: fsc Stream.scala

package adpro

sealed trait Stream[+A] {
  import Stream._

  def headOption () :Option[A] =
    this match {
      case Empty => None
      case Cons(h,t) => Some(h())
    }

  def tail :Stream[A] = this match {
      case Empty => Empty
      case Cons(h,t) => t()
  }

  def foldRight[B] (z : =>B) (f :(A, =>B) => B) :B = this match {
      case Empty => z
      case Cons (h,t) => f (h(), t().foldRight (z) (f))
      // Note 1. f can return without forcing the tail
      // Note 2. this is not tail recursive (stack-safe) It uses a lot of stack
      // if f requires to go deeply into the stream. So folds sometimes may be
      // less useful than in the strict case
    }

  // Note 1. eager; cannot be used to work with infinite streams. So foldRight
  // is more useful with streams (somewhat opposite to strict lists)
  def foldLeft[B] (z : =>B) (f :(A, =>B) =>B) :B = this match {
      case Empty => z
      case Cons (h,t) => t().foldLeft (f (h(),z)) (f)
      // Note 2. even if f does not force z, foldLeft will continue to recurse
    }

  def exists (p : A => Boolean) :Boolean = this match {
      case Empty => false
      case Cons (h,t) => p(h()) || t().exists (p)
      // Note 1. lazy; tail is never forced if satisfying element found this is
      // because || is non-strict
      // Note 2. this is also tail recursive (because of the special semantics
      // of ||)
    }


  //Exercise 2
  def toList: List[A] = this match {
    case Empty => List[A]()
    case Cons (h,t) => h() :: t().toList
  }

  //Exercise 3
  def take(n: Int): Stream[A] = this match {
    case Empty => Stream.empty
    case Cons(h,t) => if (n==0) Stream.empty else Stream.cons(h(), t().take(n-1))
  }

  def drop(n: Int): Stream[A] = this match {
    case Empty => Stream.empty[A]
    case Cons (_,t) => if(n == 0) this else t().drop(n-1)
  }
  
  //Exercise 4
  def takeWhile(p: A => Boolean): Stream[A] = this match {
    case Empty => Stream.empty
    case Cons(h,t) => if(p(h())) Stream.cons(h(), t().takeWhile(p)) else Stream.empty
  }


  //Exercise 5
  def forAll(p: A => Boolean): Boolean = this match {
    case Empty => true
    case Cons(h,t) => if(p(h())) t().forAll(p) else false
  }


  //Exercise 6
  def takeWhile2(p: A => Boolean): Stream[A] =
    this.foldRight[Stream[A]] (Stream.empty) ((x: A, acc) => if(p(x)) Stream.cons(x, acc) else acc)


  //Exercise 7
  def headOption2 () :Option[A] = 
    this.foldRight[Option[A]] (None) ((x: A,acc)=> if(x==Empty) return acc else return Some(x))

  //Exercise 8 The types of these functions are omitted as they are a part of the exercises
  def map[B] (f: A => B) :Stream[B] =
    this.foldRight[Stream[B]] (Stream.empty[B]) ((x: A, acc) => Stream.cons(f(x), acc))

  def filter (p: A => Boolean):Stream[A] =
    this.foldRight (Stream.empty[A]) ((x: A, acc) => if(p(x)) Stream.cons(x, acc) else acc)

  def append[B >: A] (that : Stream[B]): Stream[B] =
    this.foldRight (that) ((x: B, acc)=> if(x==Empty) acc else Stream.cons(x, acc))

  def flatMap[B] (f: A => Stream[B]): Stream[B] =
    this.foldRight (Stream.empty[B]) ((x: A, acc) => if(x==Empty) acc else f(x).foldRight (acc) (Stream.cons(_, _))) 


  //Exercise 09
  //Put your answer here:
  /*
  The implementation is efficient with stream because only the elements up until headOption (the first occurrence with filter=true) would be computed with the filter.
  This is due to the lazy property which avoids computations before they are relevant.
  This is the reason why lists would not suit this implementation, since all elements would be applied the filter.
   */

  //Exercise 10
  //Put your answer here:
  def fibs: Stream[Int] = {
    def fibsRec(a: Int) (b: Int) : Stream[Int] = Stream.cons(a, fibsRec (b) (a+b))
    fibsRec (0) (1)
  } 

  //Exercise 11
  def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] =
    f(z) match {
      case Some((a,s)) => Stream.cons(a, unfold (s) (f))
      case None => Stream.empty
    }

  //Exercise 12
  def fib2 = unfold (0,1) ((s: (Int, Int)) => s match {
    case (x,y) => Some(x, (y, x+y))
  })
  def from2(n: Int) = unfold (n) ((s: Int) => Some(s, s+1))

  //Exercise 13
  def map2[B] (f: A => B) :Stream[B] = unfold (this) ((s: Stream[A]) => s match {
    case Cons(h, t) => Some(f (h()), t())
    case _ => None
  })

  def take2 (n: Int) : Stream[A] = unfold (this, n) ((s: (Stream[A], Int)) => s match {
    case (_, 0) => None
    case (Cons(h,t), m) => Some((h(), (t(), m-1)))
    case _ => None
  })

  def takeWhile3 (p: A => Boolean): Stream[A] = unfold (this) ((s: Stream[A]) => s match {
    case Cons(h,t) => if(p(h())) Some(h(), t()) else None
    case _ => None
  })

  def zipWith[B,C] (f: (A,B) => C) (v: Stream[B]) : Stream[C] =
    unfold ((this, v)) ((s : (Stream[A], Stream[B])) => s match {
      case (Cons(ah,at),Cons(bh,bt)) => Some((f (ah(),bh()), (at(),bt())))
      case _ => None
    })

  /*
    === Exercise 13.b ===
    The result of this should be a list of size 10 containing a series of "true" booleans.
    These results are formed using the 'or' operator "||" between the two streams,
    which vary between true and false. While the first stream is false, the second stream
    is true - as such, the product is always true.
  */

}

case object Empty extends Stream[Nothing]
case class Cons[+A](h: ()=>A, t: ()=>Stream[A]) extends Stream[A]

object Stream {

  def empty[A]: Stream[A] = Empty

  def cons[A] (hd: => A, tl: => Stream[A]) :Stream[A] = {
    lazy val head = hd
    lazy val tail = tl
    Cons(() => head, () => tail)
  }

  def apply[A] (as: A*) :Stream[A] =
    if (as.isEmpty) empty
    else cons(as.head, apply(as.tail: _*))
    // Note 1: ":_*" tells Scala to treat a list as multiple params
    // Note 2: pattern matching with :: does not seem to work with Seq, so we
    //         use a generic function API of Seq


  //Exercise 1
  def from(n:Int):Stream[Int]=cons(n,from(n+1))

  def to(n:Int):Stream[Int]= if(n == 0) Stream.empty else Stream.cons(n,to(n-1))

  val naturals: Stream[Int] = from(0)


}

