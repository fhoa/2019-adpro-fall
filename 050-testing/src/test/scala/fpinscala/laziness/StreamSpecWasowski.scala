// wasowski, Advanced Programming, IT University of Copenhagen
package fpinscala.laziness
import scala.language.higherKinds

import org.scalatest.{FreeSpec, Matchers}
import org.scalatest.prop.PropertyChecks
import org.scalacheck.Gen
import org.scalacheck.Arbitrary
import org.scalacheck.Arbitrary.arbitrary

// If you comment out all the import lines below, then you test the Scala
// Standard Library implementation of Streams. Interestingly, the standard
// library streams are stricter than those from the book, so some laziness tests
// fail on them.

//import stream00._    // uncomment to test the book solution
//import stream01._ // uncomment to test the broken headOption implementation
import stream02._ // uncomment to test another version that breaks headOption

class StreamSpecWasowski extends FreeSpec with Matchers with PropertyChecks {

  import Stream._

  // A simple converter of lists to streams
  def list2stream[A] (la :List[A]): Stream[A] =
    la.foldRight (Stream.empty[A]) (cons[A](_,_))

  def createNLengthStream[A] (acc : Stream[A]) (n: Int) : Stream[A] =
    if (n <= 0) acc else createNLengthStream (cons(Gen.asInstanceOf[A], acc)) (n-1)

  // note that there is a name clash between Stream.empty and the testing
  // library, so we need to qualify Stream.empty

  // An example generator of random finite non-empty streams
  // (we use the built in generator of lists and convert them to streams,
  // using the above converter)
  //
  // 'suchThat' filters out the generated instances that do not satisfy the
  // predicate given in the right argument.
  def genNonEmptyStream[A] (implicit arbA :Arbitrary[A]) :Gen[Stream[A]] =
    for {
      la <- arbitrary[List[A]] suchThat { _.nonEmpty }
    } yield list2stream (la)

  def genInfiniteStream[A] (implicit arbA :Arbitrary[A]) :Gen[Stream[A]] = {
    def genInfiniteRec (v : A) : Stream[A] =
      cons(v, genInfiniteRec(v))
    arbitrary map {genInfiniteRec(_) }

  }

  "cons" - {
    "cons returns content (01)" in {
      Stream.cons(1, Stream.empty) should not be Stream.empty
    }
  }

  "headOption" - {

    // a scenario test:

    "returns None on an empty Stream (01)" in {
      (Stream.empty.headOption) shouldBe (None)
    }

    // two property tests:

    "returns the head of a singleton stream packaged in Some (02)" in {
      forAll { (n :Int) => cons (n, Stream.empty).headOption should be (Some (n)) }
    }

    "returns the head of random stream packaged in Some (02)" in {
      // The implict makes the generator available in the context
      implicit def arbIntStream = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])

      // This property uses our generator of non empty streams thanks to the
      // above implicit declaration
      forAll { (s :Stream[Int]) => s.headOption shouldNot be (None) }
    }


    "headOption should not force the tail of the stream - test that infinite stream does not fail (03)" in {
      //Make infinite stream
      implicit def arbIntInfStream = Arbitrary[Stream[Int]] (genInfiniteStream[Int])

      //Assert no failure
      try {
        forAll { (s :Stream[Int]) => s.headOption }
      }
      catch {
        //In case of stackoverflow
        case _ : Throwable  => fail()
      }
    }

    "headOption should not force the tail of the stream (03)" in {
        (cons(0, fail())).headOption
    }

  }

  "take" - {
    "take should should not force tail element on stream (01)" in {
        (cons(0, fail())).take(2)
    }

    "take should should not force head element on  stream(01)" in {
        (cons(fail(), Stream.empty)).take(2)
    }

    "take(n) does not force (n+1)st head ever (even if we force all elements of take(n)) (02)" in {
        (createNLengthStream(cons(fail(), Stream.empty))(100)).take(100)
      }

      "s.take(n).take(n) == s.take(n) for any Stream s and any n (idempotency) (03)" in {
        implicit def arbIntStream = Arbitrary[Stream[Int]](genNonEmptyStream[Int])

        forAll { (s: Stream[Int]) => s.take(10).take(10) == s.take(10) }

      }
    }

  "drop" - {
    "s.drop(n).drop(m) == s.drop(n+m) for any n, m (additivity) (01) - changing the stream" in {
      implicit def arbIntStream = Arbitrary[Stream[Int]](genNonEmptyStream[Int])

      forAll { (s: Stream[Int]) => s.drop(10).drop(20) == s.drop(30) }
    }

    "s.drop(n).drop(m) == s.drop(n+m) for any n, m (additivity) (01) - changing n, m, stream" in {
      implicit def arbIntStream = Arbitrary[Stream[Int]](genNonEmptyStream[Int])

      forAll { (s: Stream[Int], n: Int, m: Int) => s.drop(n).drop(m) == s.drop(n + m) }
    }

    "s.drop(n) does not force any of the dropped elements heads (02) - Scenario" in {
        cons(fail(), cons(fail(), Stream.empty)).drop(2)
    }

    "s.drop(n) does not force any of the dropped elements heads (02) - Property testing" in {
        implicit def arbIntStream = Arbitrary[Stream[Int]](genNonEmptyStream[Int])
        forAll { (s: Stream[Int], n: Int) => cons(fail(), s).drop(n) }
    }

    "the above should hold even if we force some stuff in the tail (03)" in {
      //TODO: WHAT TO DOOO
    }
}

  "map" - {
    "x.map(id) == x (where id is the identity function) (01)" in {
      implicit def arbIntInfStream = Arbitrary[Stream[Int]](genNonEmptyStream[Int])

      //Assert no failure
      forAll { (s: Stream[Int]) => s.map[Int]((i:Int)=>i) == s}
    }

    "map terminates on infinite streams (02)" in {
      try {
        //Make infinite stream
        implicit def arbIntInfStream = Arbitrary[Stream[Int]](genInfiniteStream[Int])

        //Assert no failure
        forAll { (s: Stream[Int]) => s.map[Int](_) }
        succeed
      }
      catch {
        //In case of stackoverflow:
        case _ : Throwable  => fail()
      }
    }
  }

  "append" - {
    "Appending element onto stream should return stream with element in final tail (01)" in {
      list2stream[Int](List[Int](1,2,3)).append(Stream.cons(4, Stream.empty[Int])) shouldBe list2stream[Int](List[Int](1,2,3,4))
    }

    "Appending stream on empty stream should be equal to the appended stream (02)" in {
      implicit def arbIntStream = Arbitrary[Stream[Int]](genNonEmptyStream[Int])

      forAll { (s: Stream[Int]) => Stream.empty[Int].append(s) shouldBe s}
    }

    "Appended stream should not have their head or tail forced (03)" in {
      implicit def arbIntStream = Arbitrary[Stream[Int]](genNonEmptyStream[Int])

      forAll { (s: Stream[Int]) => s.append(fail())}
    }

    "Appending on a stream should not force tail of that stream (04)" in {
      implicit def arbIntStream = Arbitrary[Stream[Int]](genNonEmptyStream[Int])

      forAll { (s: Stream[Int]) => Stream.cons(1, fail()).append(s)}
    }
  }



}
