// Group number: 27
//
// AUTHOR1: KRWA
// TIME1: 14 <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)
//
// AUTHOR2: FHOA
// TIME2: 14 <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)
//
// AUTHOR3: SEOL
// TIME3: 14 <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)
// You should work with the file by following the associated exercise sheet
// (available in PDF from the course website).


// Advanced Programming. Andrzej Wasowski. IT University
// To execute this example, run "sbt run" or "sbt test" in the root dir of the project
// Spark needs not to be installed (sbt takes care of it)

import Main.{ReviewEmbedded, TokenizedReviews}
import org.apache.spark.ml.classification.MultilayerPerceptronClassifier
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.ml.feature.Tokenizer
import org.apache.spark.sql.{Dataset, Row, SparkSession}
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.ml.linalg.{Vector, Vectors}
import org.apache.spark.ml.tuning.{CrossValidator, ParamGridBuilder}

// This
// (https://stackoverflow.com/questions/40015416/spark-unable-to-load-native-hadoop-library-for-your-platform)
// actually does seems to work, to eliminate the missing hadoop message.
// 'WARN NativeCodeLoader: Unable to load native-hadoop library for your platform... using builtin-java classes where applicable'
// AW not sure if the 'hadoop missing warning' matters though.

object Main {

	type Embedding = (String, List[Double])
	type ParsedReview = (Integer, String, Double)
  type JoinedReviewWithCount = (Integer, Double, Array[Double], Integer)
  type ReviewEmbedded = (Integer, Double, Array[Double])
  type TokenizedReviews = (Integer, Seq[String], Double)
  type finalReviewEmbedded = (Integer, Double, Vector)


	org.apache.log4j.Logger getLogger "org"  setLevel (org.apache.log4j.Level.WARN)
	org.apache.log4j.Logger getLogger "akka" setLevel (org.apache.log4j.Level.WARN)
	val spark =  SparkSession.builder
		.appName ("Sentiment")
		.master  ("local[5]")
		.getOrCreate

	spark.conf.set("spark.executor.memory", "4g")

  import spark.implicits._

	val reviewSchema = StructType(Array(
			StructField ("reviewText", StringType, nullable=false),
			StructField ("overall",    DoubleType, nullable=false),
			StructField ("summary",    StringType, nullable=false)))

	// Read file and merge the text and summary into a single text column

	def loadReviews (path: String): Dataset[ParsedReview] =
		spark
			.read
			.schema (reviewSchema)
			.json (path)
			.rdd
			.zipWithUniqueId
			.map[(Integer,String,Double)] { case (row,id) =>
          (id.toInt, s"${row getString 2} ${row getString 0}", row getDouble 1) }
			.toDS
			.withColumnRenamed ("_1", "id" )
			.withColumnRenamed ("_2", "text")
			.withColumnRenamed ("_3", "overall")
			.as[ParsedReview]

  // Load the GLoVe embeddings file

  def loadGlove (path: String): Dataset[Embedding] =
		spark
			.read
			.text (path)
      .map  { _ getString 0 split " " }
      .map  (r => (r.head, r.tail.toList.map (_.toDouble))) // yuck!
			.withColumnRenamed ("_1", "word" )
			.withColumnRenamed ("_2", "vec")
			.as[Embedding]

  def getTokenizedReviews (reviews : Dataset[ParsedReview]) : Dataset[TokenizedReviews] =
  {
    val tokenizer = new Tokenizer().setInputCol("text").setOutputCol("words")
    tokenizer.transform(reviews).select("id", "words", "overall").as[TokenizedReviews]
  }

  def flattenReviews (reviews : Dataset[TokenizedReviews]) : Dataset[ParsedReview] =
  {
    def flatten (review: TokenizedReviews)  =
      review._2.map ((v:String) => (review._1, v, review._3))
    reviews.flatMap[ParsedReview] (flatten(_))
  }

  def reduceReviewsByKey (review: Dataset[JoinedReviewWithCount]):Dataset[ReviewEmbedded] =
    review.map (row => (row._1,(row._2,row._3, row._4)))
      .groupByKey(_._1)
      .reduceGroups((a, b) => {
        val (aoverall: Double, avec: Array[Double], acount: Integer) = a._2
        val (boverall: Double, bvec: Array[Double], bcount: Integer) = b._2
        val acc = avec.zip(bvec).map { case (x, y) => x + y }
        (a._1, (aoverall, acc, acount+bcount))
      })
      .map[ReviewEmbedded]( (v : (Integer, (Integer, (Double, Array[Double], Integer)))) => {
        val (id: Integer, (overall: Double, vec: Array[Double], count: Integer)) = v._2
        (id, overall, vec.map(_/count))
      })

  def mapRating (reviews: Dataset[ReviewEmbedded]) : Dataset[ReviewEmbedded] = {
    reviews.map((review:ReviewEmbedded) => (review._1, Math.round((review._2 * 3) / 5) - 1, review._3))
  }

  def mapFeatureToVector (reviews:Dataset[ReviewEmbedded]) : Dataset[finalReviewEmbedded] = {
    def getRatingTuple(review: ReviewEmbedded): finalReviewEmbedded =
      (review._1, review._2, Vectors.dense(review._3))
    reviews.map[finalReviewEmbedded] (getRatingTuple(_))
  }

  def main(args: Array[String]) = {

    val DATA_PATH = "/data/"

    val glove  = loadGlove (s"${DATA_PATH}/glove.6B.50d.txt")
    val reviews = loadReviews (s"${DATA_PATH}/Office_Products_5.json")

    // ================= replace the following with the project code ======================
    
    //   - First clean the data
    //      - Use the tokenizer to turn records with reviews into records with
    //      lists of words
		//         documentation: https://spark.apache.org/docs/latest/ml-features.html#tokenizer
    //         output type: a collection of (Integer, Seq[String], Double)
    println("Started tokenizing reviews")
    val tokenizedReviews = getTokenizedReviews(reviews)


    // ================= Second translate the reviews to embeddings =======================
    //      - Flatten the list to contain single words
    //         output type: a collection (Integer, String, Double) but much
    //         longer than input
    println("Started flattening dataset")
    val flattendTokenizedReview  = flattenReviews(tokenizedReviews)


    //      - Join the glove vectors with the triples
    //         output type: a collection (Integer, String, Double,
    //         Array[Double])
    //      - Drop the word column, we don't need it anymore
    //         output type: a collection (Integer, Double, Array[Double])
    println("Started joining glove and review tables")
    val joinedDS : Dataset[Row] =
      flattendTokenizedReview.join(glove, $"_2" === $"word").drop("_2").drop("word")


    //      - Add a column of 1s
    //         output type: a collection (Integer, Double, Array[Double], Integer)
    println("Adding count column")
    val joinedDSExtraCol : Dataset[JoinedReviewWithCount] =
      joinedDS.withColumn("count", lit(1)).as[JoinedReviewWithCount]


    //      - Reduce By Key (using the first or two first columns as Key), summing the last column
    //         output type: a collection (Integer, Double, Array[Double], Integer)
    //         (just much shorter this time)
    //      - In each row divide the Array (vector) by the count (the last column)
    //         output type: a collection (Integer, Double, Array[Double])
    //         This is the input for the classifier training
    println("Summing / reducing by key")
    /* Spark cannot getAs[Array] (trash)*/
    val summed : Dataset[ReviewEmbedded] = reduceReviewsByKey(joinedDSExtraCol)


    // =================== Train the sentiment perceptron here (this is *one* possible workflow, and it is slow ...) ====================
    //
    //  - Train the perceptron:
    //      - translated the ratings from 1..5 to 1..3 (use map)
    println("Started mapping reviews from 1..5 to 0..2")
    val translatedRatings = mapRating(summed)

    println("Convert feature into vector")
    val data = mapFeatureToVector(translatedRatings)

    //      - make sure tha columns are named "id", "label", "features"
    println("Renaming columns")
    val dataRenamed = data
      .withColumnRenamed ("_1", "id" )
      .withColumnRenamed ("_2", "label")
      .withColumnRenamed ("_3", "features")
      .as[finalReviewEmbedded]


    //      - follow the MultilayerPerceptronClassifier tutorial.
    println("Splitting data into training and test data")
    val splits = dataRenamed.randomSplit(Array(0.6, 0.4), seed = 1234L)
    val train = splits(0)
    val test = splits(1)

    //      - Remember that the first layer needs to be #50 (for vectors of size
    //      50), and the last needs to be #3.
    //  - Validate the perceptron
    //      - Either implement your own validation loop  or use
    //        org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
    //
    val layers = Array[Int](50, 5, 4, 3)

    println("Create trainer")
    // create the trainer and set its parameters
    val trainer = new MultilayerPerceptronClassifier()
      .setLayers(layers)
      .setBlockSize(128)
      .setSeed(1234L)
      .setMaxIter(10)

    val paramGrid = new ParamGridBuilder().build()
    val evaluator = new MulticlassClassificationEvaluator().setMetricName("accuracy")
    val cv = new CrossValidator().setEstimator(trainer) // our MultiLayerPerceptronClassifier
                                 .setEvaluator(evaluator)
                                 .setEstimatorParamMaps(paramGrid)
                                 .setParallelism(4)
                                 .setNumFolds(10)
    println("Train model")
    val model = cv.fit(train)
    println("Compute accuracy")
    val result = model.transform(test) // query the result objects for accuracy
    val predictionAndLabels = result.select("prediction", "label")
    println(s"Test set accuracy = ${evaluator.evaluate(predictionAndLabels)}")

    // Any suggestions of improvement to the above guide are welcomed by
    // teachers.
    //
    // This is an open programming exercise, you do not need to follow the above
    // guide to complete it.


    // ANSWERS TO QUESTIONS:
    // 1. We have managed to run the following:
    //    - Amazon_Instant_Video_5.json - 26,8 MB - 1254 s to run
    //    - Musical_Instruments_5.json - 7,1 MB - 2095 s to run
    //    - Office_Products_5.json - 53,3 MB - 2607 s to run
    // 2. The output and set values for the different files can be found below:
    //    -  Amazon_Instant_Video_5
    //        accuracy: 0.5617809245013275
    //        iterations: 10
    //        layer configuration: (50, 5, 4, 3)
    //    -  Musical_Instruments_5
    //        accuracy: 0.6722166998011928
    //        iterations: 10
    //        layer configuration: (50, 5, 4, 3)
    //    -  Office_Products_5
    //        accuracy: 0.5688637013048862
    //        iterations: 10
    //        layer configuration: (50, 5, 4, 3)
    // 3. We did not have the time to do this exercise - and were kind of uncertain what was asked of us.
    // 4. We did not have time to implement any extensions

		spark.stop
  }

}
