// Group number: 27
//
// AUTHOR1: KRWA
// TIME1: 4 <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)
//
// AUTHOR2: FHOA
// TIME2: 4 <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)
//
// AUTHOR3: SEOL
// TIME3: 4 <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)
// You should work with the file by following the associated exercise sheet
// (available in PDF from the course website).



package adpro
import java.util.concurrent._
import scala.language.implicitConversions

// Work through the file top-down, following the exercises from the week's
// sheet.  Uncomment and complete code fragments.

object Par {

  type Par[A] = ExecutorService => Future[A]
  def run[A] (s: ExecutorService) (a: Par[A]) : Future[A] = a(s)


  case class UnitFuture[A] (get: A) extends Future[A] {
    def isDone = true
    def get (timeout: Long, units: TimeUnit) = get
    def isCancelled = false
    def cancel (evenIfRunning: Boolean) : Boolean = false
  }

  def unit[A] (a: A) :Par[A] = (es: ExecutorService) => UnitFuture(a)

  def map2[A,B,C] (a: Par[A], b: Par[B]) (f: (A,B) => C) : Par[C] =
    (es: ExecutorService) => {
      val af = a (es)
      val bf = b (es)
      UnitFuture (f(af.get, bf.get))
    }

  def fork[A] (a: => Par[A]) : Par[A] = es => es.submit(
    new Callable[A] { def call = a(es).get }
  )

  def lazyUnit[A] (a: =>A) : Par[A] = fork(unit(a))

  // Exercise 1 (CB7.4)

  def asyncF[A,B] (f: A => B) : A => Par[B] = a =>
    map2 (lazyUnit(a), unit()) ((aa,_) => f(aa))

  // map is shown in the book

  def map[A,B] (pa: Par[A]) (f: A => B) : Par[B] =
    map2 (pa,unit (())) ((a,_) => f(a))

  // Exercise 2 (CB7.5)

  def sequence[A] (ps: List[Par[A]]): Par[List[A]] =
    (es: ExecutorService) => {
      val ls = ps.map(a => a(es).get())
      UnitFuture(ls)
    }

  // Exercise 3 (CB7.6)

  // this is shown in the book:

  // def parMap[A,B](ps: List[A])(f: A => B): Par[List[B]] = fork {
  //   val fbs: List[Par[B]] = ps.map(asyncF(f))
  //   sequence(fbs)
  // }

  def parFilter[A](as: List[A])(f: A => Boolean): Par[List[A]] = fork {
    val ls = as map asyncF((a) => if(f(a)) List(a) else List())
    sequence(ls.flatten)
  }

  // Exercise 4: implement map3 using map2

  def map3[A,B,C,D] (pa :Par[A], pb: Par[B], pc: Par[C]) (f: (A,B,C) => D) :Par[D] = {
    val fa = (a: A, b: B) => (c: C) => f(a, b, c)
    val fb : Par[C => D] = map2(pa, pb) (fa)
    map2 (fb, pc) ((fc : C => D, c : C) => fc(c))
  }

  // shown in the book

  // def equal[A](e: ExecutorService)(p: Par[A], p2: Par[A]): Boolean = p(e).get == p2(e).get

  // Exercise 5 (CB7.11)

  def choiceN[A] (n: Par[Int]) (choices: List[Par[A]]) : Par[A] = (es: ExecutorService) => {
    val index = n(es).get()
    choices(index)(es)
  }

  def choice[A] (cond: Par[Boolean]) (t: Par[A], f: Par[A]) : Par[A] = {
    val choices = List[Par[A]](t,f)
    val n = map (cond) (b => if(b) 1 else 0)
    choiceN (n) (choices)
  }

  // Exercise 6 (CB7.13)

  def chooser[A,B] (pa: Par[A]) (choices: A => Par[B]): Par[B] = (es: ExecutorService) => {
    map [A, B] (pa) (a => choices(a)(es).get())(es)
  }

  def choiceNviaChooser[A] (n: Par[Int]) (choices: List[Par[A]]) :Par[A] =
    chooser (n) (c => choices(c))

  def choiceViaChooser[A] (cond: Par[Boolean]) (t: Par[A], f: Par[A]) : Par[A] =
    chooser (cond) (b => if(b) t else f)

  // Exercise 7 (CB7.14)

  def join[A] (a : Par[Par[A]]) :Par[A] = (es: ExecutorService) => {
    a(es).get()(es) //We might have misunderstood something I'm afraid
  }

  //"Can you see how to implement flatMap using join?"
  //Yes - we may use join on each element Par[Par[A]] in a List[Par[Par[A]] as to produce a List[Par[A]]

  //"And can you implement join using flatMap?"
  //Yes - we may have a list only containing our a:Par[Par[A]] in which we provide a function (a:Par[Par[A]]) => (es:ExecutorService) => a(es).get()(es)

  //"Compare the type of join with the type of List.flatten (and the relation of join to chooser against the relation of List.flatten to List.flatMap)."
  //They are much the same - instead of wrapping elements in Lists of List we instead wrap elements in Pars of Par.
  //This is apparent in the function signature - they have the same relationship in each respective context.

  class ParOps[A](p: Par[A]) {

  }

  implicit def toParOps[A](p: Par[A]): ParOps[A] = new ParOps(p)
}
