// Advanced Programming, A. Wąsowski, IT University of Copenhagen
//
// Group number: 27
//
// AUTHOR1: Frederik Andersen, fhoa
// TIME1: 4,5 <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)
//
// AUTHOR2: Sebastian Olsen, seol
// TIME2: 4,5 <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)
//
// AUTHOR3: Kristoffer Astrup, krwa
// TIME2: 4,5 <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)
//
// You should work with the file by following the associated exercise sheet
// (available in PDF from the course website).
//
// This file is compiled with 'sbt compile' and tested with 'sbt test'.

package adpro

trait RNG {
  def nextInt: (Int, RNG)
}

object RNG {

  case class SimpleRNG (seed: Long) extends RNG {
    def nextInt: (Int, RNG) = {
      val newSeed = (seed * 0x5DEECE66DL + 0xBL) & 0xFFFFFFFFFFFFL // `&` is bitwise AND. We use the current seed to generate a new seed.
      val nextRNG = SimpleRNG (newSeed) // The next state, which is an `RNG` instance created from the new seed.
      val n = (newSeed >>> 16).toInt // `>>>` is right binary shift with zero fill. The value `n` is our new pseudo-random integer.
      (n, nextRNG) // The return value is a tuple containing both a pseudo-random integer and the next `RNG` state.
    }
  }

  // Exercise 1 (CB 6.1)

  def nonNegativeInt (rng: RNG): (Int, RNG) = {
    val (v, rng2) = rng.nextInt
    if(v == Int.MinValue) (0,rng2)
    else (math.abs(v), rng2)
    //(v << 1 >>> 1, rng2)
  }

  // Exercise 2 (CB 6.2)

  def double (rng: RNG): (Double, RNG) = {
    val (v, rng2) = rng.nextInt
    (((v.toDouble / Int.MaxValue.toDouble)+1)/2.0d, rng2)
  }
  /*
  After pondering we have concluded that the distribution generated
  by the function is in fact uniform.
   */

  // Exercise 3 (CB 6.3)

  def intDouble (rng: RNG): ((Int, Double), RNG) = {
    val (v, rng2) = nonNegativeInt(rng)
    val (d, rng3) = double(rng2)
    ((v, d), rng3)
  }

  def doubleInt (rng: RNG): ((Double, Int), RNG) = {
    val (v, rng2) = nonNegativeInt(rng)
    val (d, rng3) = double(rng2)
    ((d, v), rng3)
  }

  def boolean (rng: RNG): (Boolean, RNG) =
    rng.nextInt match { case (i,rng2) => (i%2==0,rng2) }

  // Exercise 4 (CB 6.4)

  def ints(count: Int) (rng: RNG): (List[Int], RNG) = {
    if(count == 0) (List[Int](), rng) else {
      val (v, rng2) = rng.nextInt
      val (xs, rng3) = ints(count-1) (rng2)
      (v :: xs, rng3)
    }
  }

  // There is something terribly repetitive about passing the RNG along
  // every time. What could we do to eliminate some of this duplication
  // of effort?

  type Rand[+A] = RNG => (A, RNG)

  val int: Rand[Int] = _.nextInt

  def unit[A](a: A): Rand[A] =
    rng => (a, rng)

  def map[A,B](s: Rand[A])(f: A => B): Rand[B] =
    rng => {
      val (a, rng2) = s(rng)
      (f(a), rng2)
    }

  def nonNegativeEven: Rand[Int] = map(nonNegativeInt)(i => i - i % 2)

  // Exercise 5 (CB 6.5) (Lazy is added so that the class does not fail
  // at load-time without your implementation).

  lazy val _double: Rand[Double] =
    map (int) (i => ((i.toDouble / Int.MaxValue.toDouble)+1)/2.0d)

  // Exercise 6 (CB 6.6)

  def map2[A,B,C](ra: Rand[A], rb: Rand[B])(f: (A, B) => C): Rand[C] = {
    rng => {
      val (a, rng2) = ra(rng)
      val (b, rng3) = rb(rng2)
      (f(a,b), rng3)
    }
  }

  // this is given in the book

  def both[A,B](ra: Rand[A], rb: Rand[B]): Rand[(A,B)] =
    map2(ra, rb)((_, _))

  lazy val randIntDouble: Rand[(Int, Double)] = both(int, double)

  lazy val randDoubleInt: Rand[(Double, Int)] = both(double, int)

  // Exercise 7 (6.7)

  def sequence[A](fs: List[Rand[A]]): Rand[List[A]] = 
    rng => {
      fs.foldRight ((List[A](), rng)) ((a, s) => {
        val (ls, rng2) = s
        val (v, rng3) = a(rng2)
        (v::ls, rng3)
      })
    }

  def _ints(count: Int): Rand[List[Int]] =
    sequence(List.fill (count) (int))

  // Exercise 8 (6.8)

  def flatMap[A,B](f: Rand[A])(g: A => Rand[B]): Rand[B] =
    rng => {
      val (v, rng2) = f(rng)
      g(v)(rng2)
    }

  def nonNegativeLessThan(n: Int): Rand[Int] =
    flatMap(nonNegativeInt) {
      i => if(i + (n-1) - (i % n) >= 0) unit(i % n)
        else nonNegativeLessThan(n)
    }

}

import State._
import adpro.RNG.SimpleRNG

import scala.collection.immutable.Stream.cons

case class State[S, +A](run: S => (A, S)) {

  // Exercise 9 (6.10)

  def map[B](f: A => B): State[S, B] =
    State(s => {
      val (a,s2) = run(s)
      (f(a), s2)
    })

  def map2[B,C](sb: State[S, B])(f: (A, B) => C): State[S, C] =
    State(s => {
      val (a, s2) = run(s)
      val (b, s3) = sb.run(s2)
      (f(a,b), s3)
    })

  def flatMap[B](f: A => State[S, B]): State[S, B] =
    State(s => {
      val (a,s2) = run(s)
      f(a).run(s2)
    })

}

object State {
  type Rand[A] = State[RNG, A]

  def unit[S, A](a: A): State[S, A] =
    State(s => (a, s))

  // Exercise 9 (6.10) continued

  def sequence[S,A](sas: List[State[S, A]]): State[S, List[A]] =
    State(s => sas.foldRight[(List[A], S)] ((List[A](), s)) ((a, acc) => {
      val (xs, s2) = acc
      val (v, s3) = a.run(s2)
      (v::xs, s3)
    }))
  // This is given in the book:

  def modify[S](f: S => S): State[S, Unit] = for {
     s <- get // Gets the current state and assigns it to `s`.
     _ <- set(f(s)) // Sets the new state to `f` applied to `s`.
  } yield ()

  def get[S]: State[S, S] = State(s => (s, s))

  def set[S](s: S): State[S, Unit] = State(_ => ((), s))

  def random_int :Rand[Int] =  State (_.nextInt)

  // Exercise 10

  def state2stream[S,A] (s :State[S,A]) (seed :S) :Stream[A] = {
    val (v, s2) = s.run(seed)
    cons(v,state2stream (s) (s2))
  }

  // Exercise 11 (lazy is added so that the class does not crash at load time
  // before you provide an implementation).

  lazy val random_integers : Stream[Int] = {
    state2stream (random_int) (SimpleRNG(42))
  }
}


// vim:cc=80:foldmethod=indent:nofoldenable
